#!/usr/bin/env python
# -*- coding: utf-8 -*-

# all the imports
import os
from flask import Flask, g


def get_app_base_path():
    return os.path.dirname(os.path.realpath(__file__))


def get_instance_folder_path():
    return os.path.join(get_app_base_path(), 'instance')


## create our little application :)
app = Flask(__name__,
	instance_path=get_instance_folder_path(),
	instance_relative_config=True)

# dummy function
def dummy_function():
	return True

# start the application
if __name__ == '__main__':
    app.run(host='0.0.0.0')
