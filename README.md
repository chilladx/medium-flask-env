My Project
==========

[![StackShare](http://img.shields.io/badge/tech-stack-0690fa.svg?style=flat)](http://stackshare.io/chilladx/medium-flask-env)

What is it?
-----------

MyProject is a fake project used as skeleton as part of a blog post: https://medium.com/@chilladx/580b4684c036.

Documentation
-------------

The documentation available is included in markdown format in the docs/
directory.

Installation
------------

Read docs/INSTALL.

Licensing
---------

Read LICENSE.
