Ansible Role: MyProjectRole
===========================

Installs myproject

Supported platforms
-------------------

Debian Jessie/64

Requirements
------------

Nginx, ElasticSearch

Role Variables
--------------

|Variable             |Values           |Note                   |
|---------------------|-----------------|-----------------------|
|myproject_environment|local or prod    |Specify the environment|
|myproject_git_branch |develop or master|The git branch         |

Dependencies
------------

None

Example Playbook
----------------

```json
- hosts: all
  roles:
    - { role: myprojectrole }
```

License
-------

MIT

Author Information
------------------

Created by Julien Andrieux
